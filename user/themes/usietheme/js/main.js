$(document).ready(function () {
	// toggle buttons handler
	// This is for every button that toggles another element. i.e. Dropdowns, Bar slides, etc.
	$('.toggle-btn').click(function () {
		// assign the target element so it can be used as a selector inside the overlay click handler.
		var target = $(this);
		// grab the selector for the element that the button activates which is contained inside a data-content atribute.
		var selector = $(this).attr('data-select');

		// if the button is being clicked and it already has the active class
		// remove the class and the overlay
		if($(target).hasClass('active')) {
			// this avoid one line of code. Equivalent to:
			// $(selector).removeClass('active');
			// $(target).removeClass('active');
			$(selector).add($(target)).removeClass('active');
			$('.overlay').remove();
		}
		// no active class found, add it and append the overlay to the body
		else {
			$(selector).add($(target)).addClass('active');
			// overlay click handler
			// remove the classes and itself if clicked 
			// pro of this technique: 
			// prevents dom click handlers while also keeping the "outside click" interation encapsulated within the first click handler, 
			// therefore making it easier to edit and mantain)
			if(!$(target).hasClass('no-overlay')) {
				$('<div class="overlay"></div>').click(function() {
			    	$(selector).add($(target)).removeClass('active');
			    	$(this).remove();
			  	}).appendTo($(document.body));
		  	}
		}
	});
});