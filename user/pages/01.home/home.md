---
title: Home
heroImage: usiebg.jpeg
HeroText1: 'A global trading company'
Services:
    -
        title: 'Purpose driven'
        icon: fa-arrow-right
        description: 'Our goal is to promote the expansion of Ethiopia and other developing countries'
    -
        title: 'Easy to work with'
        icon: fa-smile-o
        description: 'Working with us is simple. Tell us what industry you work in and what kind of supplier you need. We’ll take care of the rest.'
    -
        title: 'Trusted Suppliers'
        icon: fa-thumbs-up
        description: 'We only work with proffesional suppliers who are at the top of their industry. They offer high quality products at competitive prices.'
    -
        title: 'Product Showcase'
        icon: button
        description: 'Check out what we have been able to source for our clients at lower costs and higher quality.'
---

